<?php

use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\HomeController::class, 'index']);
Route::any('json/data',[App\Http\Controllers\SiswaController::class, 'getListKota'])->name('json.data');

Route::group(['prefix' => 'siswa', 'as' => 'siswa.'], function () {
    Route::get('/',[App\Http\Controllers\SiswaController::class, 'index']);
    Route::get('/json',[App\Http\Controllers\SiswaController::class, 'getSiswa'])->name('json');
    Route::get('/tambah',[App\Http\Controllers\SiswaController::class, 'create'])->name('tambah');
    Route::post('/simpan',[App\Http\Controllers\SiswaController::class, 'store'])->name('simpan');
    Route::get('/edit/{id}',[App\Http\Controllers\SiswaController::class, 'edit'])->name('edit');
    Route::get('/detail/{id}',[App\Http\Controllers\SiswaController::class, 'show'])->name('detail');
    Route::get('/update/{id}',[App\Http\Controllers\SiswaController::class, 'update'])->name('update');
    Route::get('/hapus/{id}',[App\Http\Controllers\SiswaController::class, 'destroy'])->name('hapus');
});

Route::group(['prefix' => 'kecamatan', 'as' => 'kecamatan.'], function () {
    Route::get('/',[App\Http\Controllers\KecamatanController::class, 'index']);
    Route::get('/json',[App\Http\Controllers\KecamatanController::class, 'getKecamatan'])->name('json');
    Route::get('/tambah',[App\Http\Controllers\KecamatanController::class, 'create'])->name('tambah');
    Route::post('/simpan',[App\Http\Controllers\KecamatanController::class, 'store'])->name('simpan');
    Route::get('/edit/{id}',[App\Http\Controllers\KecamatanController::class, 'edit'])->name('edit');
    Route::get('/detail/{id}',[App\Http\Controllers\KecamatanController::class, 'show'])->name('detail');
    Route::get('/update/{id}',[App\Http\Controllers\KecamatanController::class, 'update'])->name('update');
    Route::get('/hapus/{id}',[App\Http\Controllers\KecamatanController::class, 'destroy'])->name('hapus');
});

Route::group(['prefix' => 'kotaKab', 'as' => 'kotaKab.'], function () {
    Route::get('/',[App\Http\Controllers\KotaKabController::class, 'index']);
    Route::get('/json',[App\Http\Controllers\KotaKabController::class, 'getKotaKab'])->name('json');
    Route::get('/tambah',[App\Http\Controllers\KotaKabController::class, 'create'])->name('tambah');
    Route::post('/simpan',[App\Http\Controllers\KotaKabController::class, 'store'])->name('simpan');
    Route::get('/edit/{id}',[App\Http\Controllers\KotaKabController::class, 'edit'])->name('edit');
    Route::get('/detail/{id}',[App\Http\Controllers\KotaKabController::class, 'show'])->name('detail');
    Route::get('/update/{id}',[App\Http\Controllers\KotaKabController::class, 'update'])->name('update');
    Route::get('/hapus/{id}',[App\Http\Controllers\KotaKabController::class, 'destroy'])->name('hapus');
});



