<?php

namespace App\Policies;

use App\Models\karyawan;
use App\Models\kecamatan;
use Illuminate\Auth\Access\HandlesAuthorization;

class KecamatanPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\karyawan  $karyawan
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function viewAny(karyawan $karyawan)
    {
        //
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\karyawan  $karyawan
     * @param  \App\Models\kecamatan  $kecamatan
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function view(karyawan $karyawan, kecamatan $kecamatan)
    {
        //
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\karyawan  $karyawan
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function create(karyawan $karyawan)
    {
        //
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\karyawan  $karyawan
     * @param  \App\Models\kecamatan  $kecamatan
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function update(karyawan $karyawan, kecamatan $kecamatan)
    {
        //
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\karyawan  $karyawan
     * @param  \App\Models\kecamatan  $kecamatan
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function delete(karyawan $karyawan, kecamatan $kecamatan)
    {
        //
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\karyawan  $karyawan
     * @param  \App\Models\kecamatan  $kecamatan
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function restore(karyawan $karyawan, kecamatan $kecamatan)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\karyawan  $karyawan
     * @param  \App\Models\kecamatan  $kecamatan
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function forceDelete(karyawan $karyawan, kecamatan $kecamatan)
    {
        //
    }
}
