<?php

namespace App\Policies;

use App\Models\karyawan;
use App\Models\siswa;
use Illuminate\Auth\Access\HandlesAuthorization;

class SiswaPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\karyawan  $karyawan
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function viewAny(karyawan $karyawan)
    {
        //
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\karyawan  $karyawan
     * @param  \App\Models\siswa  $siswa
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function view(karyawan $karyawan, siswa $siswa)
    {
        //
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\karyawan  $karyawan
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function create(karyawan $karyawan)
    {
        //
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\karyawan  $karyawan
     * @param  \App\Models\siswa  $siswa
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function update(karyawan $karyawan, siswa $siswa)
    {
        //
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\karyawan  $karyawan
     * @param  \App\Models\siswa  $siswa
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function delete(karyawan $karyawan, siswa $siswa)
    {
        //
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\karyawan  $karyawan
     * @param  \App\Models\siswa  $siswa
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function restore(karyawan $karyawan, siswa $siswa)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\karyawan  $karyawan
     * @param  \App\Models\siswa  $siswa
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function forceDelete(karyawan $karyawan, siswa $siswa)
    {
        //
    }
}
