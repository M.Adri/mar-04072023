<?php

namespace App\Policies;

use App\Models\barang;
use App\Models\karyawan;
use Illuminate\Auth\Access\HandlesAuthorization;

class BarangPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\karyawan  $karyawan
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function viewAny(karyawan $karyawan)
    {
        //
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\karyawan  $karyawan
     * @param  \App\Models\barang  $barang
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function view(karyawan $karyawan, barang $barang)
    {
        //
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\karyawan  $karyawan
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function create(karyawan $karyawan)
    {
        //
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\karyawan  $karyawan
     * @param  \App\Models\barang  $barang
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function update(karyawan $karyawan, barang $barang)
    {
        //
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\karyawan  $karyawan
     * @param  \App\Models\barang  $barang
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function delete(karyawan $karyawan, barang $barang)
    {
        //
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\karyawan  $karyawan
     * @param  \App\Models\barang  $barang
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function restore(karyawan $karyawan, barang $barang)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\karyawan  $karyawan
     * @param  \App\Models\barang  $barang
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function forceDelete(karyawan $karyawan, barang $barang)
    {
        //
    }
}
