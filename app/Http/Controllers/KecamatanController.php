<?php

namespace App\Http\Controllers;

use App\Models\kecamatan;
use App\Models\kotaKab;
use App\Http\Requests\StorekecamatanRequest;
use App\Http\Requests\UpdatekecamatanRequest;
use Illuminate\Support\Facades\Route;
use Carbon\Carbon;
use DataTables;
use Validator;
use Illuminate\Http\Request;

class KecamatanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['kec'] = kecamatan::All();
        $data['url'] = Route::currentRouteName();
        return view('kecamatan/index',$data);
    }

    public function getKecamatan(){
        $data = kecamatan::get();
        return DataTables::of($data)
        ->addColumn('kota',function($data){
            $kota = kotaKab::find($data->id_kotaKab);
            return $kota->nama;
        })
        ->addColumn('action', function($data){
            $actionBtn = '<div class="d-flex justify-content-center "><a href="/kecamatan/detail/'.$data->id_kecamatan.'" class="edit btn btn-success btn-sm align-items-center col-3 h-50"><i class="bi-eye" style="font-size: 10px"></i>Detail</a><a href="/kecamatan/edit/'.$data->id_kecamatan.'" class="edit btn btn-primary mx-2 h-50 btn-sm align-items-center col-3"><i class="bi-pencil-square" style="font-size: 10px"></i>Edit</a><a onclick="return confirm(`Apakah anda yakin ?`)" href="/kecamatan/hapus/'.$data->id_kecamatan.'" class="edit btn btn-danger h-50 btn-sm align-items-center col-3"><i class="bi-trash" style="font-size: 10px"></i>Delete</a></div>';
            return $actionBtn;
            })
        ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $date= Carbon::now();
        $data['date']=$date->format('d/m/Y');
        $data['url'] = Route::currentRouteName();
        $data['kotaKab'] = kotaKab::All();
        return view('kecamatan/tambah',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StorekecamatanRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'name' => 'required',
            'id_kotaKab'=>'required',
        ]);

        if($validate->fails()){
            return redirect()->route('kecamatan.')->with('after_save',  [
                'alert'     => 'danger',
                'icon'      => 'attention',
                'title'     => 'Error !',
                'text-1'    => '',
                'text-2'    => 'Gagal Menambahkan Data'
            ]
            );
        }

        $after_save = [
            'alert'     => 'success',
            'icon'      => 'check',
            'title'     => 'Success ! ',
            'text-1'    => '',
            'text-2'    => 'Data Baru Berhasil Ditambahkan'
        ];
        
        $data = new kecamatan();
        $data->id_kecamatan;
        $data->nama= $request->name;
        $data->id_kotaKab = $request->id_kotaKab;
        $data->save();
        return redirect()->route('kecamatan.')->with('after_save', $after_save);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\kecamatan  $kecamatan
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['kecamatan']=kecamatan::find($id);
        $data['url'] = Route::currentRouteName();
        $date= Carbon::now();
        $data['date']=$date->format('d/m/Y');
        return view('kecamatan.detail',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\kecamatan  $kecamatan
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['kecamatan']=kecamatan::find($id);
        $data['url'] = Route::currentRouteName();
        $date= Carbon::now();
        $data['kotaKab']=kotaKab::All();
        $data['date']=$date->format('d/m/Y');
        return view('kecamatan.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdatekecamatanRequest  $request
     * @param  \App\Models\kecamatan  $kecamatan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = Validator::make($request->all(), [
            'name' => 'required',
            'id_kotaKab'=>'required',
        ]);

        if($validate->fails()){
            return redirect()->route('kecamatan.')->with('after_save',  [
                'alert'     => 'danger',
                'icon'      => 'attention',
                'title'     => 'Error !',
                'text-1'    => '',
                'text-2'    => 'Gagal Menambahkan Data'
            ]
            );
        }

        $after_save = [
            'alert'     => 'success',
            'icon'      => 'check',
            'title'     => 'Success ! ',
            'text-1'    => '',
            'text-2'    => 'Data Berhasil Diperbarui'
        ];
        
        $data = kecamatan::find($id);
        $data->nama= $request->name;
        $data->id_kotaKab = $request->id_kotaKab;
        $data->save();
        return redirect()->route('kecamatan.')->with('after_save', $after_save);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\kecamatan  $kecamatan
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = kecamatan::find($id);
        $data->delete();
    return redirect()->route('kecamatan.')->with('after_save', [
        'alert'     => 'success',
        'icon'      => 'check',
        'title'     => 'Success ! ',
        'text-1'    => '',
        'text-2'    => 'Data Berhasil Dihapus'
    ]);
    }
}
