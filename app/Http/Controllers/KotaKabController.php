<?php

namespace App\Http\Controllers;

use App\Models\kotaKab;
use App\Http\Requests\StorekabRequest;
use App\Http\Requests\UpdatekabRequest;
use Illuminate\Support\Facades\Route;
use Carbon\Carbon;
use DataTables;
use Validator;
use Illuminate\Http\Request;

class KotaKabController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['kotaKab'] = kotaKab::All();
        $data['url'] = Route::currentRouteName();
        return view('kota/index',$data);
    }

    public function getKotaKab(){
        $data = kotaKab::get();
        return DataTables::of($data)
        ->addColumn('action', function($data){
            $actionBtn = '<div class="d-flex justify-content-center "><a href="/kotaKab/detail/'.$data->id_kotaKab.'" class="edit btn btn-success btn-sm align-items-center col-3 h-50"><i class="bi-eye" style="font-size: 10px"></i>Detail</a><a href="/kotaKab/edit/'.$data->id_kotaKab.'" class="edit btn btn-primary mx-2 h-50 btn-sm align-items-center col-3"><i class="bi-pencil-square" style="font-size: 10px"></i>Edit</a><a onclick="return confirm(`Apakah anda yakin ?`)" href="/kotaKab/hapus/'.$data->id_kotaKab.'" class="edit btn btn-danger h-50 btn-sm align-items-center col-3"><i class="bi-trash" style="font-size: 10px"></i>Delete</a></div>';
            return $actionBtn;
            })
        ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $date= Carbon::now();
        $data['date']=$date->format('d/m/Y');
        $data['url'] = Route::currentRouteName();
        return view('kota/tambah',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StorekabRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        if($validate->fails()){
            return redirect()->route('kotaKab.')->with('after_save',  [
                'alert'     => 'danger',
                'icon'      => 'attention',
                'title'     => 'Error !',
                'text-1'    => '',
                'text-2'    => 'Gagal Menambahkan Data'
            ]
            );
        }

        $after_save = [
            'alert'     => 'success',
            'icon'      => 'check',
            'title'     => 'Success ! ',
            'text-1'    => '',
            'text-2'    => 'Data Baru Berhasil Ditambahkan'
        ];
        
        $data = new kotaKab();
        $data->id_kotaKab;
        $data->nama= $request->name;
        $data->save();
        return redirect()->route('kotaKab.')->with('after_save', $after_save);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\kota\kab  $kab
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['kotaKab']=kotaKab::find($id);
        $data['url'] = Route::currentRouteName();
        $date= Carbon::now();
        $data['date']=$date->format('d/m/Y');
        return view('kota.detail',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\kota\kab  $kab
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['kotaKab']=kotaKab::find($id);
        $data['url'] = Route::currentRouteName();
        $date= Carbon::now();
        $data['date']=$date->format('d/m/Y');
        return view('kota.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdatekabRequest  $request
     * @param  \App\Models\kota\kab  $kab
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        if($validate->fails()){
            return redirect()->route('kotaKab.')->with('after_save',  [
                'alert'     => 'danger',
                'icon'      => 'attention',
                'title'     => 'Error !',
                'text-1'    => '',
                'text-2'    => 'Gagal Menambahkan Data'
            ]
            );
        }

        $after_save = [
            'alert'     => 'success',
            'icon'      => 'check',
            'title'     => 'Success ! ',
            'text-1'    => '',
            'text-2'    => 'Data Berhasil Diperbarui'
        ];
        
        $data = kotaKab::find($id);
        $data->nama= $request->name;
        $data->save();
        return redirect()->route('kotaKab.')->with('after_save', $after_save);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\kota\kab  $kab
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = kotaKab::find($id);
        $data->delete();
    return redirect()->route('kotaKab.')->with('after_save', [
        'alert'     => 'success',
        'icon'      => 'check',
        'title'     => 'Success ! ',
        'text-1'    => '',
        'text-2'    => 'Data Berhasil Dihapus'
    ]);
    }
}
