<?php

namespace App\Http\Controllers;

use App\Models\siswa;
use App\Models\kecamatan;
use App\Models\kotaKab;
use App\Http\Requests\StoresiswaRequest;
use App\Http\Requests\UpdatesiswaRequest;
use Illuminate\Support\Facades\Route;
use Carbon\Carbon;
use DataTables;
use Validator;
use Illuminate\Http\Request;

class SiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['siswa'] = siswa::All();
        $data['url'] = Route::currentRouteName();
        return view('siswa/index',$data);
    }

    public function getSiswa(){
        $data = siswa::get();
        return DataTables::of($data)
        ->addColumn('kota',function($data){
            $kota = kotaKab::find($data->id_kotaKab);
            return $kota->nama;
        })
        ->addColumn('kecamatan',function($data){
            $kecamatan = kecamatan::find($data->id_kecamatan);
            return $kecamatan->nama;
        })
        ->addColumn('action', function($data){
            $actionBtn = '<div class="d-flex justify-content-center "><a href="/siswa/detail/'.$data->id_siswa.'" class="edit btn btn-success btn-sm align-items-center col-4 h-50"><i class="bi-eye" style="font-size: 10px"></i>Detail</a><a href="/siswa/edit/'.$data->id_siswa.'" class="edit btn btn-primary mx-2 h-50 btn-sm align-items-center col-4"><i class="bi-pencil-square" style="font-size: 10px"></i>Edit</a><a onclick="return confirm(`Apakah anda yakin ?`)" href="/siswa/hapus/'.$data->id_siswa.'" class="edit btn btn-danger h-50 btn-sm align-items-center col-4"><i class="bi-trash" style="font-size: 10px"></i>Delete</a></div>';
            return $actionBtn;
            })
        ->toJson();
    }

    public function getListKota(Request $request){
        $kecamatan = kecamatan::where('id_kotaKab','=',$request->id)->get();
        return response()->json([
            'status'    => true,
            'message'   => 'Data Successfully Loaded.',
            'data'      => $kecamatan
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $date= Carbon::now();
        $data['date']=$date->format('d/m/Y');
        $data['url'] = Route::currentRouteName();
        $data['kotaKab'] = kotaKab::All();
        return view('siswa/tambah',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoresiswaRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'name' => 'required',
            'id_kotaKab'=>'required',
            'id_kecamatan'=>'required',
            'alamat'=>'required',
        ]);

        if($validate->fails()){
            return redirect()->route('siswa.')->with('after_save',  [
                'alert'     => 'danger',
                'icon'      => 'attention',
                'title'     => 'Error !',
                'text-1'    => '',
                'text-2'    => 'Gagal Menambahkan Data'
            ]
            );
        }

        $after_save = [
            'alert'     => 'success',
            'icon'      => 'check',
            'title'     => 'Success ! ',
            'text-1'    => '',
            'text-2'    => 'Data Baru Berhasil Ditambahkan'
        ];
        
        $data = new siswa();
        $data->id_siswa;
        $data->nama= $request->name;
        $data->id_kotaKab = $request->id_kotaKab;
        $data->id_kecamatan = $request->id_kecamatan;
        $data->alamat = $request->alamat;
        $data->save();
        return redirect()->route('siswa.')->with('after_save', $after_save);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\siswa  $siswa
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['siswa']=siswa::find($id);
        $data['url'] = Route::currentRouteName();
        $date= Carbon::now();
        $data['date']=$date->format('d/m/Y');
        return view('siswa.detail',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\siswa  $siswa
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['siswa']=siswa::find($id);
        $data['url'] = Route::currentRouteName();
        $date= Carbon::now();
        $data['date']=$date->format('d/m/Y');
        $data['kotaKab'] = kotaKab::All();
        return view('siswa.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdatesiswaRequest  $request
     * @param  \App\Models\siswa  $siswa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $validate = Validator::make($request->all(), [
            'name' => 'required',
            'id_kotaKab'=>'required',
            'id_kecamatan'=>'required',
            'alamat'=>'required',
        ]);

        if($validate->fails()){
            return redirect()->route('siswa.')->with('after_save',  [
                'alert'     => 'danger',
                'icon'      => 'attention',
                'title'     => 'Error !',
                'text-1'    => '',
                'text-2'    => 'Gagal Menambahkan Data'
            ]
            );
        }

        $after_save = [
            'alert'     => 'success',
            'icon'      => 'check',
            'title'     => 'Success ! ',
            'text-1'    => '',
            'text-2'    => 'Data Baru Berhasil Ditambahkan'
        ];
        
        $data = siswa::find($id);
        $data->nama= $request->name;
        $data->id_kotaKab = $request->id_kotaKab;
        $data->id_kecamatan = $request->id_kecamatan;
        $data->alamat = $request->alamat;
        $data->save();
        return redirect()->route('siswa.')->with('after_save', $after_save);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\siswa  $siswa
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = siswa::find($id);
        $data->delete();
        return redirect()->route('siswa.')->with('after_save', [
        'alert'     => 'success',
        'icon'      => 'check',
        'title'     => 'Success ! ',
        'text-1'    => '',
        'text-2'    => 'Data Berhasil Dihapus'
    ]);
    }
}
