<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class kecamatan extends Model
{
    use HasFactory;
    protected $table = 'kecamatan';
    
    protected $primaryKey = 'id_kecamatan';

    protected $fillable = [
        'id_kecamatan',
        'nama',
        'id_kotaKab',
    ];

    public $incrementing = true;

    public function kotaKab()
    {
        return $this->belongsTo('App\Models\kotaKab', 'id_kotaKab');
    }

    public function siswa()
    {
        return $this->hasMany('App\Models\siswa','id_kecamatan');
    }
}
