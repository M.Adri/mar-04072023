<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class siswa extends Model
{
    use HasFactory;
    protected $table = 'siswa';
    
    protected $primaryKey = 'id_siswa';

    protected $fillable = [
        'id_siswa',
        'id_kecamatan',
        'nama',
        'id_kotaKab',
        'alamat',
    ];

    public $incrementing = true;

    public function kotaKab()
    {
        return $this->belongsTo('App\Models\kotaKab', 'id_kotaKab');
    }

    public function kecamatan()
    {
        return $this->belongsTo('App\Models\kecamatan', 'id_kecamatan');
    }
}
