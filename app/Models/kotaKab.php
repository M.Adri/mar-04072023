<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class kotaKab extends Model
{
    use HasFactory;
    protected $table = 'kotaKab';
    
    protected $primaryKey = 'id_kotaKab';

    protected $fillable = [
        'id_kotaKab',
        'nama',
    ];

    public $incrementing = true;

    public function kecamatan()
    {
        return $this->hasMany('App\Models\kecamatan','id_kotaKab');
    }

    public function siswa()
    {
        return $this->hasMany('App\Models\siswa','id_kotaKab');
    }
}
