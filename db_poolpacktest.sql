--
-- PostgreSQL database dump
--

-- Dumped from database version 15.1
-- Dumped by pg_dump version 15rc2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: db_poolpacktest; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE db_poolpacktest WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE_PROVIDER = libc LOCALE = 'English_United States.1252';


ALTER DATABASE db_poolpacktest OWNER TO postgres;

\connect db_poolpacktest

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: failed_jobs; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.failed_jobs (
    id bigint NOT NULL,
    uuid character varying(255) NOT NULL,
    connection text NOT NULL,
    queue text NOT NULL,
    payload text NOT NULL,
    exception text NOT NULL,
    failed_at timestamp(0) without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


ALTER TABLE public.failed_jobs OWNER TO postgres;

--
-- Name: failed_jobs_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.failed_jobs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.failed_jobs_id_seq OWNER TO postgres;

--
-- Name: failed_jobs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.failed_jobs_id_seq OWNED BY public.failed_jobs.id;


--
-- Name: kecamatan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.kecamatan (
    id_kecamatan integer NOT NULL,
    nama character varying(50),
    "id_kotaKab" integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.kecamatan OWNER TO postgres;

--
-- Name: kecamatan_id_kecamatan_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.kecamatan_id_kecamatan_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.kecamatan_id_kecamatan_seq OWNER TO postgres;

--
-- Name: kecamatan_id_kecamatan_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.kecamatan_id_kecamatan_seq OWNED BY public.kecamatan.id_kecamatan;


--
-- Name: kotaKab; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."kotaKab" (
    "id_kotaKab" integer NOT NULL,
    nama character varying(50),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public."kotaKab" OWNER TO postgres;

--
-- Name: kotaKab_id_kotaKab_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."kotaKab_id_kotaKab_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."kotaKab_id_kotaKab_seq" OWNER TO postgres;

--
-- Name: kotaKab_id_kotaKab_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."kotaKab_id_kotaKab_seq" OWNED BY public."kotaKab"."id_kotaKab";


--
-- Name: migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.migrations (
    id integer NOT NULL,
    migration character varying(255) NOT NULL,
    batch integer NOT NULL
);


ALTER TABLE public.migrations OWNER TO postgres;

--
-- Name: migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.migrations_id_seq OWNER TO postgres;

--
-- Name: migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.migrations_id_seq OWNED BY public.migrations.id;


--
-- Name: password_resets; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.password_resets (
    email character varying(255) NOT NULL,
    token character varying(255) NOT NULL,
    created_at timestamp(0) without time zone
);


ALTER TABLE public.password_resets OWNER TO postgres;

--
-- Name: personal_access_tokens; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.personal_access_tokens (
    id bigint NOT NULL,
    tokenable_type character varying(255) NOT NULL,
    tokenable_id bigint NOT NULL,
    name character varying(255) NOT NULL,
    token character varying(64) NOT NULL,
    abilities text,
    last_used_at timestamp(0) without time zone,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.personal_access_tokens OWNER TO postgres;

--
-- Name: personal_access_tokens_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.personal_access_tokens_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.personal_access_tokens_id_seq OWNER TO postgres;

--
-- Name: personal_access_tokens_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.personal_access_tokens_id_seq OWNED BY public.personal_access_tokens.id;


--
-- Name: siswa; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.siswa (
    id_siswa integer NOT NULL,
    nama character varying(50),
    "id_kotaKab" integer,
    id_kecamatan integer,
    alamat character varying(50),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.siswa OWNER TO postgres;

--
-- Name: siswa_id_siswa_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.siswa_id_siswa_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.siswa_id_siswa_seq OWNER TO postgres;

--
-- Name: siswa_id_siswa_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.siswa_id_siswa_seq OWNED BY public.siswa.id_siswa;


--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    email_verified_at timestamp(0) without time zone,
    password character varying(255) NOT NULL,
    remember_token character varying(100),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.users OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: failed_jobs id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.failed_jobs ALTER COLUMN id SET DEFAULT nextval('public.failed_jobs_id_seq'::regclass);


--
-- Name: kecamatan id_kecamatan; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.kecamatan ALTER COLUMN id_kecamatan SET DEFAULT nextval('public.kecamatan_id_kecamatan_seq'::regclass);


--
-- Name: kotaKab id_kotaKab; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."kotaKab" ALTER COLUMN "id_kotaKab" SET DEFAULT nextval('public."kotaKab_id_kotaKab_seq"'::regclass);


--
-- Name: migrations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.migrations ALTER COLUMN id SET DEFAULT nextval('public.migrations_id_seq'::regclass);


--
-- Name: personal_access_tokens id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.personal_access_tokens ALTER COLUMN id SET DEFAULT nextval('public.personal_access_tokens_id_seq'::regclass);


--
-- Name: siswa id_siswa; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.siswa ALTER COLUMN id_siswa SET DEFAULT nextval('public.siswa_id_siswa_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Data for Name: failed_jobs; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: kecamatan; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.kecamatan (id_kecamatan, nama, "id_kotaKab", created_at, updated_at) VALUES
	(1, 'Bandung Timur', 1, '2023-07-04 06:44:29', '2023-07-04 06:44:47'),
	(2, 'Cimahi Utara', 2, '2023-07-04 06:44:38', '2023-07-04 06:45:22'),
	(3, 'Padalarang', 3, '2023-07-04 06:45:33', '2023-07-04 06:45:33'),
	(4, 'Cimahi Tengah', 2, '2023-07-04 06:45:43', '2023-07-04 06:45:43'),
	(5, 'Antapani', 1, '2023-07-04 06:45:51', '2023-07-04 06:45:51'),
	(6, 'Lembang', 3, '2023-07-04 06:46:01', '2023-07-04 06:46:01'),
	(7, 'Cimahi Selatan', 2, '2023-07-04 06:46:10', '2023-07-04 06:46:10'),
	(8, 'Batujajar', 3, '2023-07-04 06:46:27', '2023-07-04 06:46:27');


--
-- Data for Name: kotaKab; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."kotaKab" ("id_kotaKab", nama, created_at, updated_at) VALUES
	(1, 'Kota Bandung', '2023-07-04 06:44:08', '2023-07-04 06:44:08'),
	(2, 'Kota Cimahi', '2023-07-04 06:44:14', '2023-07-04 06:44:14'),
	(3, 'Kab. Bandung Barat', '2023-07-04 06:44:19', '2023-07-04 06:44:19');


--
-- Data for Name: migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.migrations (id, migration, batch) VALUES
	(22, '2014_10_12_000000_create_users_table', 1),
	(23, '2014_10_12_100000_create_password_resets_table', 1),
	(24, '2019_08_19_000000_create_failed_jobs_table', 1),
	(25, '2019_12_14_000001_create_personal_access_tokens_table', 1),
	(26, '2023_07_04_013521_create_siswas_table', 1),
	(27, '2023_07_04_014144_create_kecamatans_table', 1),
	(28, '2023_07_04_014323_create_kabs_table', 1);


--
-- Data for Name: password_resets; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: personal_access_tokens; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: siswa; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.siswa (id_siswa, nama, "id_kotaKab", id_kecamatan, alamat, created_at, updated_at) VALUES
	(1, 'Agus', 1, 1, 'Alamat Siswa', '2023-07-04 06:46:44', '2023-07-04 06:46:44'),
	(2, 'Budi', 2, 2, 'Alamat Siswa', '2023-07-04 06:46:59', '2023-07-04 06:46:59'),
	(3, 'Nana', 3, 3, 'Alamat Siswa', '2023-07-04 06:47:13', '2023-07-04 06:47:13'),
	(4, 'Bambang', 3, 3, 'Alamat Siswa', '2023-07-04 06:47:23', '2023-07-04 06:47:23'),
	(5, 'Fitri', 2, 2, 'Alamat Siswa', '2023-07-04 06:47:41', '2023-07-04 06:47:41'),
	(6, 'Bagus', 1, 1, 'Alamat Siswa', '2023-07-04 06:47:56', '2023-07-04 06:47:56'),
	(7, 'Hartoko', 1, 1, 'Alamat Siswa', '2023-07-04 06:48:08', '2023-07-04 06:48:08'),
	(8, 'Dadan', 3, 3, 'Alamat Siswa', '2023-07-04 06:48:25', '2023-07-04 06:48:25'),
	(9, 'Ceceng', 2, 2, 'Alamat Siswa', '2023-07-04 06:48:41', '2023-07-04 06:48:41'),
	(10, 'Ilham', 1, 1, 'Alamat Siswa', '2023-07-04 06:49:00', '2023-07-04 06:49:00'),
	(11, 'Iqbal', 3, 3, 'Alamat Siswa', '2023-07-04 06:49:15', '2023-07-04 06:49:15'),
	(12, 'Adi', 2, 2, 'Alamat Siswa', '2023-07-04 06:49:29', '2023-07-04 06:49:29');


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: failed_jobs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.failed_jobs_id_seq', 1, false);


--
-- Name: kecamatan_id_kecamatan_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.kecamatan_id_kecamatan_seq', 8, true);


--
-- Name: kotaKab_id_kotaKab_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."kotaKab_id_kotaKab_seq"', 3, true);


--
-- Name: migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.migrations_id_seq', 28, true);


--
-- Name: personal_access_tokens_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.personal_access_tokens_id_seq', 1, false);


--
-- Name: siswa_id_siswa_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.siswa_id_siswa_seq', 12, true);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_id_seq', 1, false);


--
-- Name: failed_jobs failed_jobs_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.failed_jobs
    ADD CONSTRAINT failed_jobs_pkey PRIMARY KEY (id);


--
-- Name: failed_jobs failed_jobs_uuid_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.failed_jobs
    ADD CONSTRAINT failed_jobs_uuid_unique UNIQUE (uuid);


--
-- Name: kecamatan kecamatan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.kecamatan
    ADD CONSTRAINT kecamatan_pkey PRIMARY KEY (id_kecamatan);


--
-- Name: kotaKab kotaKab_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."kotaKab"
    ADD CONSTRAINT "kotaKab_pkey" PRIMARY KEY ("id_kotaKab");


--
-- Name: migrations migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);


--
-- Name: personal_access_tokens personal_access_tokens_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.personal_access_tokens
    ADD CONSTRAINT personal_access_tokens_pkey PRIMARY KEY (id);


--
-- Name: personal_access_tokens personal_access_tokens_token_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.personal_access_tokens
    ADD CONSTRAINT personal_access_tokens_token_unique UNIQUE (token);


--
-- Name: siswa siswa_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.siswa
    ADD CONSTRAINT siswa_pkey PRIMARY KEY (id_siswa);


--
-- Name: users users_email_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_email_unique UNIQUE (email);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: password_resets_email_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX password_resets_email_index ON public.password_resets USING btree (email);


--
-- Name: personal_access_tokens_tokenable_type_tokenable_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX personal_access_tokens_tokenable_type_tokenable_id_index ON public.personal_access_tokens USING btree (tokenable_type, tokenable_id);


--
-- PostgreSQL database dump complete
--

