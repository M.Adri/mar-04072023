<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <script src="{{ asset('js/app.js') }}"></script>

    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('assets') }}/css/jquery.dataTables.min.css" rel="stylesheet" />
    <script src="{{ asset('dist') }}/js/sweetalert2.js"></script>

    <style>
        .main {
            width: 100%;
            background-color: rgb(244, 248, 255);
        }

        i {
            margin-right: 5px;
        }

        i.item-delete {
            margin-right: 0px;
        }

        a {
            text-decoration: none;
        }

        .buttonAction {
            margin-left: 5px;
        }

        #body-row {
            margin-left: 0;
            margin-right: 0;
        }

        #sidebar-container {
            min-height: 100vh;
            background-color: white;
            padding: 0;
        }

        .title {
            font-weight: bold;
        }

        .dashboard {
            font-size: 1.5em;
        }

        .sidebar-expanded {
            width: 230px;
        }

        #sidebar-container .list-group a {
            height: 50px;
        }

        #sidebar-container .list-group .sidebar-submenu a {
            height: 45px;
            padding-left: 30px;
        }

        .sidebar-submenu {
            font-size: 0.9rem;
            background-color: white;
        }

        .sidebar-separator-title {
            background-color: white;
        }

        .list-group-item-action {
            background-color: white;
            height: 25px;
        }

        #sidebar-container .list-group .list-group-item[aria-expanded="false"] .submenu-icon::after {
            font-family: 'Bootstrap-icons';
            content: "\F229";
            display: inline;
            text-align: right;
            padding-left: 10px;
        }

        #sidebar-container .list-group .list-group-item[aria-expanded="true"] .submenu-icon::after {
            font-family: 'Bootstrap-icons';
            content: "\F238";
            display: inline;
            text-align: right;
            padding-left: 10px;
        }

        .add-items {
            margin-right: 10px;
        }

        .card-item {
            margin-right: 15px;
        }
    </style>
</head>

<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                    data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                    aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav me-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ms-auto">
                        <!-- Authentication Links -->
                        {{-- @guest
                        @if (Route::has('login'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                        @endif

                        @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                        @endif
                        @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                                data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->nama_karyawan }}
                            </a>

                            <div class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </div>
                        </li>
                        @endguest --}}
                    </ul>
                </div>
            </div>
        </nav>
        <div class="d-flex">
            <nav class="shadow-sm">
                <div class="row shadow-sm" id="body-row">
                    <div id="sidebar-container" class="sidebar-expanded d-none d-md-block">
                        <ul class="list-group">
                            <li
                                class="list-group-item sidebar-separator-title text-muted d-flex align-items-center menu-collapsed">
                                <a href="{{url('/')}}" class="text-dark title dashboard">
                                    <i class="bi-house-door"></i>Dashboard</a>
                            </li>
                            <a href="#submenu1" data-toggle="collapse" aria-expanded="false"
                                class=" list-group-item list-group-item-action flex-column align-items-start">
                                <div class="d-flex w-100 justify-content-start align-items-center">
                                    <span class="menu-collapsed title"></i>Manajamen Data</span>
                                    <span class="submenu-icon ml-auto"></span>
                                </div>
                            </a>
                            <div id='submenu1' class="collapse sidebar-submenu">
                                <a href="{{url('siswa')}}" class="list-group-item list-group-item-action ">
                                    <span class="menu-collapsed"> <i class="bi-person"></i>Data Siswa</span>
                                </a>
                                <a href="{{url('kecamatan')}}" class="list-group-item list-group-item-action ">
                                    <span class="menu-collapsed"> <i class="bi-buildings"></i>Data Kecamatan</span>
                                </a>
                                <a href="{{url('kotaKab')}}" class="list-group-item list-group-item-action ">
                                    <span class="menu-collapsed"> <i class="bi-building"></i>Data Kota/Kabupaten</span>
                                </a>
                            </div>
                        </ul>
                    </div>

                </div>
            </nav>
            <main class="main py-4">
                @yield('content')
            </main>
        </div>
        <script src="{{ asset('assets') }}/libs/jquery/dist/jquery.min.js"></script>
        <script src="{{ asset('assets') }}/libs/jquery/dist/jquery.mask.min.js"></script>
        <script src="{{ asset('assets') }}/libs/bootstrap/dist/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="{{ asset('assets') }}/js/dataTables/jquery.dataTables.min.js"></script>
        @stack('scripts')

</body>

</html>