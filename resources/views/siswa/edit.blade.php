@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-11">
            @if (Session::has('after_save'))
            <div class="col-md-12" style="margin-top:20px;">
                <div class="alert alert-xs alert-dismissible mx-3 alert-{{ Session::get('after_save.alert') }}">
                    <button type="button" class="btn btn-outline-success close m-0 " data-dismiss="alert">x</button>
                    <strong>{{ Session::get('after_save.title') }}</strong>
                    <a href="javascript:void(0)" class="alert-link">{{ Session::get('after_save.text-1') }}</a> {{
                    Session::get('after_save.text-2') }}
                </div>
            </div>
            @endif
            <div class="d-flex justify-content-between mb-3 align-items-center">
                <div class="flex-column">
                    <h2 class="mb-0">Tambah Data</h2>
                    <ol class="breadcrumb mb-0">
                        @foreach (explode('.',$url) as $p)
                        <li class="breadcrumb-item"><a href="#" @if ($loop->last)
                                class='text-warning'
                                @endif>{{$p}}</a></li>
                        @endforeach
                    </ol>
                </div>
                <a class="btn btn-sm btn-primary align-items-center d-flex h-50 rounded-5" href="javascripy:void(0)"><i
                        class="bi-calendar-fill p-0" style="font-size: 15px"></i><span>{{$date}}</span></a>
            </div>
            <div class="card">
                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    <form action="/siswa/update/{{$siswa->id_siswa}}" method="PUT">
                        @csrf
                        <div class="row">
                            <div class="form-group col-12 my-2">
                                <label for="name">Nama Siswa</label>
                                <div class="input-group">
                                    <input type="text" name="name" id="name" class="form-control"
                                        value="{{$siswa->nama}}" placeholder="Masukkan Nama Siswa" required>
                                </div>
                            </div>
                            <div class="form-group col-12 my-2">
                                <label for="id_kecamatan">Nama Kota/Kabupaten</label>
                                <select class="form-control text-dark" id="id_kecamatan" name="id_kecamatan">
                                    <option value="">Pilih Kota</option>
                                    @foreach ($kotaKab as $p)
                                    <option value="{{$p->id_kotaKab}}">{{$p->nama}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-12 my-2">
                                <label for="id_kota">Nama Kecamatan</label>
                                <select class="form-control text-dark" id="id_kota" name="id_kotaKab">
                                    {{-- @foreach ($kotaKab as $p)
                                    <option value="{{$p->id_kotaKab}}">{{$p->nama}}</option>
                                    @endforeach --}}
                                </select>
                            </div>
                            <div class="form-group col-12 my-2">
                                <label for="alamat">Alamat</label>
                                <div class="input-group">
                                    <textarea type="text" name="alamat" id="alamat" class="form-control"
                                        value="{{$siswa->alamat}}" placeholder="Masukkan Alamat" required></textarea>
                                </div>
                            </div>
                            <div class="col-12 d-flex form-row justify-content-end align-items-center mt-4">
                                <a href="/siswa" class="btn btn-danger mx-2">Batal</a>
                                <button type="submit" class="btn btn-success mx-0" value="submit">Simpan</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script src="{{ asset('assets') }}/login/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{{ asset('assets') }}/libs/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

<script>
    $(document).ready(function() {
        $(document).on('change', '#id_kecamatan', function() {
        var id_kota = $(this).val();
        var div = $(this).parent();
        var op = " ";
        $.ajax({
            type: 'get',
            url: '{{route("json.data")}}',
            data: {'id':id_kota},
            success: function(json){
                console.log(json);
                        for (var i = 0; i < json.data.length; i++){
                    op += '<option class="option" value="'+json.data[i].id_kotaKab+'">'+json.data[i].nama+'</option>';
                }
                $("#id_kota").empty();
                $("#id_kota").append(op);
            },
            error: function(){
                console.log('success');
            },
        });
    }); 
});
</script>
@endpush