@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-11">
            @if (Session::has('after_save'))
            <div class="col-md-12" style="margin-top:20px;">
                <div class="alert alert-xs alert-dismissible mx-3 alert-{{ Session::get('after_save.alert') }}">
                    <button type="button" class="btn btn-outline-success close m-0 " data-dismiss="alert">x</button>
                    <strong>{{ Session::get('after_save.title') }}</strong>
                    <a href="javascript:void(0)" class="alert-link">{{ Session::get('after_save.text-1') }}</a> {{
                    Session::get('after_save.text-2') }}
                </div>
            </div>
            @endif
            <div class="d-flex justify-content-between mb-3 align-items-center">
                <div class="flex-column">
                    <h2 class="mb-0">Daftar Kota</h2>
                    <ol class="breadcrumb mb-0">
                        @foreach (explode('.',$url) as $p)
                        <li class="breadcrumb-item"><a href="#" @if ($loop->last)
                                class='text-warning'
                                @endif>{{$p}}</a></li>
                        @endforeach
                    </ol>
                </div>
                <a class="btn btn-sm btn-primary align-items-center d-flex h-50 rounded-5"
                    href="{{route('kotaKab.tambah') }}"><i class="bi-plus p-0" style="font-size: 15px"></i><span>Tambah
                        Data</span></a>
            </div>
            <div class="card">
                <div class="card-body">
                    <table class="table my-3 mx-0 w-100" id="tabel-kota">
                        <thead class="text-white thead text-center">
                            <tr>
                                <th id="headTable">Id Kota/Kabupaten</th>
                                <th id="headTable">Kota/Kabupaten</th>
                                <th id="headTable">Aksi</th>
                            </tr>
                        </thead>
                        <tbody class="text-dark text-center">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')

<script type="text/javascript">
    $(document).ready(function(){
        renderDataTable();
    });
    function renderDataTable(){
        $('#tabel-kota').DataTable({
            pagingType: "full_numbers",
            lengthMenu: [[ 5, 15, 25, 100, -1 ], [ 5, 15, 25, 100, "All" ]],
            processing:true,
            serverSide:true,
            ajax:'{{ route("kotaKab.json") }}',
            columns:[
                    {
                        data:'id_kotaKab',
                        name:'id_kotaKab'
                    },
                    {
                        data:'nama',
                        name:'nama'
                    },
                    {
                        data:'action',
                        name:'action'
                    },
            ]
        })
    };
</script>

@endpush