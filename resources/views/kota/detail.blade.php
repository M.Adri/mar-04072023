@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-11">
            @if (Session::has('after_save'))
            <div class="col-md-12" style="margin-top:20px;">
                <div class="alert alert-xs alert-dismissible mx-3 alert-{{ Session::get('after_save.alert') }}">
                    <button type="button" class="btn btn-outline-success close m-0 " data-dismiss="alert">x</button>
                    <strong>{{ Session::get('after_save.title') }}</strong>
                    <a href="javascript:void(0)" class="alert-link">{{ Session::get('after_save.text-1') }}</a> {{
                    Session::get('after_save.text-2') }}
                </div>
            </div>
            @endif
            <div class="d-flex justify-content-between mb-3 align-items-center">
                <div class="flex-column">
                    <h2 class="mb-0">Detail Kota</h2>
                    <ol class="breadcrumb mb-0">
                        @foreach (explode('.',$url) as $p)
                        <li class="breadcrumb-item"><a href="#" @if ($loop->last)
                                class='text-warning'
                                @endif>{{$p}}</a></li>
                        @endforeach
                    </ol>
                </div>
                <a class="btn btn-sm btn-primary align-items-center d-flex h-50 rounded-5" href="javascripy:void(0)"><i
                        class="bi-calendar-fill p-0" style="font-size: 15px"></i><span>{{$date}}</span></a>
            </div>
            <div class="card">
                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    <form>
                        @csrf
                        <div class="row">
                            <div>
                                <div class="w-100">
                                    <div class="form-group col-12 my-2">
                                        <label for="id">id Kota/Kabupaten</label>
                                        <div class="input-group">
                                            <input type="text" name="id" id="name" class="form-control" placeholder=""
                                                value="{{$kotaKab->id_kotaKab}}" readonly>
                                        </div>
                                    </div>
                                    <div class="form-group col-12 my-2">
                                        <label for="name">Nama Kota/Kabupaten</label>
                                        <div class="input-group">
                                            <input type="text" name="name" id="name" class="form-control"
                                                placeholder="Masukkan Nama Kota/Kabupaten" value="{{$kotaKab->nama}}"
                                                readonly>
                                        </div>
                                    </div>
                                    <div class="d-flex col-12 align-items-center">
                                        <div class="col-12 d-flex form-row justify-content-end align-items-center mt-4">
                                            <a href="/kotaKab" class="btn btn-danger mx-2">Kembali</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script src="{{ asset('assets') }}/login/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{{ asset('assets') }}/libs/bootstrap/dist/js/bootstrap.min.js"></script>
@endpush